import React, { useReducer } from 'react';

import proyectoContext from '../proyectos/proyectoContext';
import proyectoReducer from '../proyectos/proyectoReducer';


const ProyectoState = props => {
    const initialState = {
      formulario : false
    };
    
    //dispatcher para ejecutar acciones
    const [state, dispatch] = useReducer(proyectoReducer, initialState)
    //serie de funcionespar el crud


    return (
        <proyectoContext.Provider
            value={{
                formulario: state.formulario
            }}>
            {props.children}
        </proyectoContext.Provider>

    )

}
 
export default ProyectoState;