import React from "react";

const Barra = () => {
  return (
    <header className="app-header">
      <p className="nombre-usuario">
        {" "}
        Hola <span>Martin</span>
      </p>
      <nav className="nav-principal">
        <a href="#!">Cessar Sesión</a>
      </nav>
    </header>
  );
};

export default Barra;
