import React, { Fragment, useContext, useState } from "react";
import proyectoContext from '../../context/proyectos/proyectoContext';

const NuevoProyecto = () => {

  //obtener el state del formulario

  const proyectosContext = useContext(proyectoContext);
  
 const { formulario } = proyectosContext;
  //state para el proyecto
  const [proyecto, guardarPoryecto] = useState({
    nombre: "",
  });

  //extreaer nombre del proyecto
  const { nombre } = proyecto;

  const onChangeForm = (e) => {
    guardarPoryecto({
      ...proyecto,
      [e.target.name]: e.target.value,
    });
  };
  const onSubmit = (e) => {
    e.preventDefault();
    //validar

    //agregar al state

    //reiniciar el form
  };
  return (
    <Fragment>
      <button type="button" className="btn btn-block btn-primario">
        Nuevo Proyecto
      </button>
      
    {formulario
      ? (
          <form className="formulario-nuevo-proyecto" onSubmit={onSubmit}>
            <input
              type="text"
              className="input-text"
              id="nombre"
              name="nombre"
              placeholder="Nombre Proyecto"
              value={nombre}
              onChange={onChangeForm}
            />

            <input
              type="submit"
              className="btn btn-block btn-primario"
              value="Agregar Proyecto"
            />
      </form>)
      : null}
      
    </Fragment>
  );
};
export default NuevoProyecto;
