import React from "react";

const FormTarea = () => {
  return (
    <div className="formulario">
      <form>
        <div className="contendero-input">
          <input
            type="text"
            className="input-text"
            placeholder="nombre tarea..."
            name="nombre"
          />
        </div>
        <div className="contendero-input">
          <input
            type="submit"
            className="btn btn-primario btn-submit btn-block"
            value="Agregar Tarea"
          />
        </div>
      </form>
    </div>
  );
};
export default FormTarea;
