import React, { Fragment } from "react";
import Tarea from "./Tarea";
const ListadoTares = () => {
  const TareasProyecto = [
    { nombre: "elegir plataforma", estado: true },
    { nombre: "elegir colores", estado: false },
    { nombre: "elegir plataforma pago", estado: false },
    { nombre: "elegir hosting", estado: true },
  ];
  return (
    <Fragment>
      <h2>proyecto</h2>
      <ul className="listado-tareas">
        {TareasProyecto.length === 0 ? (
          <li className="tarea">
            <p>no hay tares</p>
          </li>
        ) : (
          TareasProyecto.map((tarea) => <Tarea tarea={tarea} />)
        )}
      </ul>
      <button className="btn btn-eliminar" type="button">
        Eliminar proyecto &times;
      </button>
    </Fragment>
  );
};

export default ListadoTares;
